This project consists of a series of scripts to perform Bayesian analysis on muon decay data of an experiment conducted at J-PARC in October 2019. The scripts are organized as follows.

Models: all .bug files are models. modelBinned and modelUnbinned allowed to understand the difference between the two kind of analysis. Then modelBinnedOnlyMinus allows to study specifically the properties of negative muons in the absorber. Due to nuclear capture their behaviour is very difference from the positive muons, that behave similarly to what happens in a vacuum.

Model running scripts: both parallelSampler scripts allow to prepare the data to be fed to the Gibbs sampler and then to run a user selected number of chains in parallel to reduce the execution time.

Plotting and chain analysis scripts: the remaining scripts are used for plotting and extraction of the final fit parameters. buildCIFromChain allows to extract the parameter's best value together with their confidence interval. plotFromChain allows to plot histograms of the parameters to see the posterior distribution. Two dimensional histograms are also produced to see the amount of correlation of the various parameters. plotFitResult produces a plot showing the binned data with their poisson uncertainty and the fitting curve (of which the contributions from positive and negative muons and the background are visible), together with a plot of the residuals.

Luca Scomparin, 15 Luglio 2020