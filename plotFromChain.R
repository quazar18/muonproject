# This program takes the output from the parallel
# sampler and produces plots of the parameters
# The chain object should be the one given as an
# output by R2jags

library(R2jags)
library(ggplot2)
library(gridExtra)
source("utils.R")

# This function produces an histogram from a vector with
# a given title and label on the x axis and returns it
oneVariableHistogram <- function(var.vec, nbins, title, x.label) {
    ggplot() +
    geom_histogram(aes(x = var.vec), bins = nbins) +
    labs(title = title, x = x.label)
}

# This function produces a 2d histogram from a vector with
# a given title and label on the axis and returns it
twoVariableHistogram <- function(vvx, vvy, nbinsx, nbinsy, title, x.label, y.label) {
    ggplot() +
    geom_bin2d(aes(x = vvx, y = vvy), bins = c(nbinsx, nbinsy)) +
    labs(title = title, x = x.label, y = y.label) +
    theme(legend.position = "none")
}

makePlots <- function(chain, outputFile = "chainPlots.pdf", corrOutput = "corrPlots.pdf") {
    # Convert the chain to mcmc class and matrix
    # for ease of use
    chain.mcmc   <- as.mcmc(chain)
    chain.matrix <- as.matrix(chain.mcmc)

    # Find out if the chain was produced in negative muon mode or not. If it was it will
    # contain the parameter delta
    negative.mode <- is.negative.mode(chain.matrix)

    # Change the names of the matrix colums in order to make them easier to use
    chain.matrix <- tidyup.col.name(chain.matrix)
    col.names <- colnames(chain.matrix)

    # Save the plots in a pdf file
    pdf(outputFile)
    
    # Plot autocorrelation function for one of the chains (the first)
    autocorr.plot(chain.mcmc[[1]], auto.layout = F)

    # Vector containing the label of each chain variable
    axis.labels <- list(p1    = "P of mu+ detection",
                        p2    = "P of mu- detection",
                        p3    = "P of background",
                        taup  = "Positive muon lifetime [s]",
                        taum  = "Negative muon lifetime [s]",
                        delta = "Increase in mu- probability")

    # Plot an histogram of each variable
    histo <- list()
    histo$p1    <- oneVariableHistogram(chain.matrix[,"p1"], 100,
                                        "Posterior of the probability of detecting a positive muon decay",
                                        axis.labels$p1)
    histo$p2    <- oneVariableHistogram(chain.matrix[,"p2"], 100,
                                        "Posterior of the probability of detecting a negative muon decay",
                                        axis.labels$p2)
    histo$p3    <- oneVariableHistogram(chain.matrix[,"p3"], 100,
                                        "Posterior of the probability of detecting a background event",
                                        axis.labels$p3)
    if(!negative.mode)
    histo$taup  <- oneVariableHistogram(chain.matrix[,"taup"], 100,
                                        "Posterior of the positive muon lifetime",
                                        axis.labels$taup)
    histo$taum  <- oneVariableHistogram(chain.matrix[,"taum"], 100,
                                        "Posterior of the negative muon lifetime",
                                        axis.labels$taum)
    if(negative.mode)
    histo$delta <- oneVariableHistogram(chain.matrix[,"delta"], 100,
                                        "Posterior of the difference of negative and positive muon probabilities",
                                        axis.labels$delta)

    if(negative.mode)
    histo$chrt  <- oneVariableHistogram(1./(chain.matrix[,"delta"] + 1.), 100,
                                        "Posterior of the muon charge ratio (positive/negative)",
                                        "Charge ratio (positive/negative)")

    lapply(histo, plot)

    # Make the 2D histogram of all the parameters against each other
    # to show all the correlations
    histo2d <- list()
    h <- 1
    for(i in 1:length(col.names)) {
        for(j in i:length(col.names)) {
            if (i == j) next
            if (negative.mode & (col.names[i] == "taup" | col.names[j] == "taup")) next
            histo2d[[h]] <- twoVariableHistogram(vvx=chain.matrix[,j], vvy=chain.matrix[,i], nbinsx=100, nbinsy=100, title="", x.label=axis.labels[[col.names[j]]], y.label=axis.labels[[col.names[i]]])
            plot(histo2d[[h]])
            h <- h + 1
        }
    }
    dev.off()

    # pdf(corrOutput, paper="a4r")
    # gl <- function(...)
    #     grid.arrange(..., layout_matrix = rbind(c( 1, 2, 3, 4),
    #                                             c(NA, 5, 6, 7),
    #                                             c(NA,NA, 8, 9),
    #                                             c(NA,NA,NA,10)))
    # do.call(gl, histo2d)


    # Close the pdf file
    # dev.off()
}