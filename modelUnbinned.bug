# This model fits data following a distribution that is
# a linear combination of two exponential distributions
# with different rates and a uniform background

model {
  tau[1] ~ dunif(1.5e-6,3e-6);
  tau[2] ~ dunif(0, 1.5e-6);
  # The uniform distribution is modelled as an exponential
  # having a very big lifetime
  tau[3] <- 1e3;

  # This calculates the probability of choosing one of the
  # three distributions.
  pin[1] ~ dunif(0.0, 0.5);
  pin[2] <- (pin[1] + delta) * tau[2] / tau[1] *
            (exp(-startRange/tau[2]) - exp(-stopRange/tau[2])) /
            (exp(-startRange/tau[1]) - exp(-stopRange/tau[1]));
  pin[3] <- 1 - pin[1] - pin[2];

  for (i in 1:ndata) {
    # One of the three distributions is extracted
    indic[i] ~ dcat(pin[]);

    # The decay constant is chosen based on the previous
    # extraction. The distribution is truncated in the
    # range of the data
    t[i] ~ dexp(1/tau[indic[i]]) T(startRange, stopRange);
  }
}
